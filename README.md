Lerner Interiors offers a wide array of exquisite window coverings, blinds, draperies and shades which are effective & functional and meet all your needs. Our goal is to make your home comfortable as well as fashionable in a way you want. Customize your window coverings & dress them up as you like.

Website: https://windowcoveringstoronto.ca
